
if [ "$1" == build -o "$1" == b ]; then
    # TODO: Auropm build
    auropm b
    exit
fi

dist=~/auro/tests/dist
module=stringmap
main=example.$module.main


mode=dump
if [ -n "$1" ]; then
    mode=$1
fi

case $mode in
    # Prints the contents and bytes of a module
    dump )
        auropm run --dump --dir $dist $main
        ;;

    # Prints the contents and bytes of a module
    upgrade )
        auropm run --upgrade --dir $dist $main
        ;;

    # Create and run a hello world module in Auro 0.8
    hello8 )
        auropm run --dir $dist $main || exit
        aurodump out
        ;;

    # Show a graph of all items and their dependencies starting from main
    dot )
        format=pdf
        graph_file=graphs/$main.$format

        tmp=/tmp/aurodump-out

        #graph_file=$tmp

        auropm run --dot --dir $dist $main > $tmp
        exitcode="$?"

        #cat $tmp; exit

        if [[ $exitcode == 0 ]]; then
            # Remove everything printed before the graph header and save it in file
            cat $tmp |
                sed -ne '/digraph/,$p' |
                dot -Kdot -T$format > $graph_file &&
                xdg-open $graph_file
        else
            cat $tmp
        fi
        ;;
esac

