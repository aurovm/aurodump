
import aurodump.items.Function;
import aurodump.items.Type;

module _array$<T> = import utils.arraylist(module{`0`=T;});
type Array$<T> = _array$<T>.``;
extend Array$<T> {
    Array$<T> new () = _array$<T>.new;
    void push (Array$<T>, T) = _array$<T>.push;
    int len (Array$<T>) = _array$<T>.len;
    T get (Array$<T>, int) = _array$<T>.get;
}

// Used as both register and its declaring var instruction
record Reg {
    int index;
    string? name;
}

// Used as both label for jumps and the instruction they jump to
type Label (int);

type HltInst (bool);

record MovInst {
    Reg dst;
    Reg src;

    // This differentiates mov instructions from dups.
    // Dup instructions declare their dst registers.
    bool isDup;
}

record JmpInst {
    Label label;
}

record JifInst {
    Label label;
    Reg cond;

    bool isNif;
}

record CallInst {
    Function function;
    Array$<Reg> args;
    Array$<Reg> outs;
}

record EndInst {
    Array$<Reg> args;
}

type Instruction (any);


extend Label {
    Label new (int r) { return Label{r}; }
    int index$get (Label this) { return this as int; }

    string toString (Label this) {
        return "lbl_" + itos(this as int);
    }
}

extend Reg {
    Reg new (int r) { return Reg{index = r, name = null string}; }

    void setName (Reg this, string name) {
        this.name = string?.new(name);
    }

    string toString (Reg this) {
        if (this.name is null) {
            return "r_" + itos(this.index);
        } else {
            return this.name.get();
        }
    }
}


extend Instruction {
    bool isCall (Instruction this) {
        return this as any is CallInst;
    }
    CallInst asCall (Instruction this) {
        return this as any as CallInst;
    }

    any inner$get (Instruction this) {
        return this as any;
    }

    Instruction hlt () {
        return Instruction{any{HltInst{false}}};
    }

    Instruction var (Reg dst) {
        return Instruction{any{dst}};
    }

    Instruction dup (Reg dst, Reg src) {
        return Instruction{any{MovInst{dst,src,true}}};
    }

    Instruction set (Reg dst, Reg src) {
        return Instruction{any{MovInst{dst,src,true}}};
    }

    Instruction jmp (Label i) {
        return Instruction{any{JmpInst{i}}};
    }

    Instruction jif (Label i, Reg cond) {
        return Instruction{any{JifInst{i, cond, false}}};
    }

    Instruction nif (Label i, Reg cond) {
        return Instruction{any{JifInst{i, cond, true}}};
    }


    string toString (Instruction this) {
        any inner = this as any;

        if (inner is Reg) {
            Reg inner = inner as Reg;
            return "var " + inner.toString();
        }

        if (inner is Label) {
            Label inner = inner as Label;
            return inner.toString() + ":";
        }

        if (inner is MovInst) {
            MovInst inner = inner as MovInst;
            return inner.dst.toString() + " = " + inner.src.toString();
        }

        if (inner is JmpInst) {
            JmpInst inner = inner as JmpInst;
            return "goto " + inner.label.toString();
        }

        if (inner is JifInst) {
            JifInst inner = inner as JifInst;
            string prefix = "if ";
            if (inner.isNif) prefix = "if !";
            return prefix + inner.cond.toString() + " goto " + inner.label.toString();
        }

        if (inner is CallInst) {
            CallInst inner = inner as CallInst;
            string s = "";
            if (inner.outs.len() > 0) {
                int i = 0;
                while (i < inner.outs.len()) {
                    if (i > 0) s = s + ", ";
                    s = s + inner.outs.get(i).toString();
                    i = i+1;
                }
                s = s + " = ";
            }

            s = s + inner.function.identifier() + "(";

            int i = 0;
            while (i < inner.args.len()) {
                if (i > 0) s = s + ", ";
                s = s + inner.args.get(i).toString();
                i = i+1;
            }
            return s + ")";
        }

        if (inner is EndInst) {
            EndInst inner = inner as EndInst;
            string s = "return ";
            int i = 0;
            while (i < inner.args.len()) {
                if (i > 0) s = s + ", ";
                s = s + inner.args.get(i).toString();
                i = i+1;
            }
            return s;
        }

        return "<UNKNOWN>";
    }
}


record Code {
    Function fn;
    Array$<Instruction> instructions;
    Array$<Reg> args;
    Array$<Reg> registers;
    Array$<Label> labels;
}

extend Code {
    Code new (Function fn) {
        return Code {
            fn = fn,
            instructions = Array$<Instruction>.new(),
            args = Array$<Reg>.new(),
            registers = Array$<Reg>.new(),
            labels = Array$<Label>.new(),
        };
    }

    CallInst addCall (Code this, Function call) {
        CallInst inner = CallInst {
            call, Array$<Reg>.new(), Array$<Reg>.new(),
        };
        this.instructions.push(Instruction{any{inner}});
        return inner;
    }

    EndInst addEnd (Code this) {
        EndInst inner = EndInst { Array$<Reg>.new() };
        this.instructions.push(Instruction{any{inner}});
        return inner;
    }

    Instruction add (Code this, Instruction inst) {
        this.instructions.push(inst);
        return inst;
    }


    Reg reg (Code this) {
        Reg reg = Reg.new(this.registers.len() + this.args.len());
        this.registers.push(reg);
        return reg;
    }

    // Must be called before any call to reg.
    // TODO: Make args be callable out of order but ending
    // in the correct index (start of register space)
    Reg arg (Code this) {
        Reg reg = Reg.new(this.args.len());
        this.args.push(reg);
        return reg;
    }

    Label lbl (Code this) {
        Label lbl = Label.new(this.labels.len());
        this.labels.push(lbl);
        return lbl;
    }



    Instruction addLabel (Code this, Label lbl) {
        return this.add(Instruction{any{lbl}});
    }



    Instruction hlt (Code this) {
        return this.add(Instruction.hlt());
    }

    Instruction var (Code this, Reg reg) {
        return this.add(Instruction.var(reg));
    }

    Instruction dup (Code this, Reg dst, Reg src) {
        return this.add(Instruction.dup(dst, src));
    }

    Instruction set (Code this, Reg dst, Reg src) {
        return this.add(Instruction.set(dst, src));
    }

    Instruction jmp (Code this, Label lbl) {
        return this.add(Instruction.jmp(lbl));
    }

    Instruction jif (Code this, Label lbl, Reg cond) {
        return this.add(Instruction.jif(lbl, cond));
    }

    Instruction nif (Code this, Label lbl, Reg cond) {
        return this.add(Instruction.nif(lbl, cond));
    }
}