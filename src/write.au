/*
Convert auro items and units to bytestreams that can be written to a file.
*/

module io = import auro.io;
module system = import auro.system;
module buffer_mod = import auro.buffer;
module string_mod = import auro.string;
module intbit = import auro.int.bit;

module arraylist_mod = import utils.arraylist;

void error (string) = system.error;


type char = string_mod.char;
int codeof (char) = string_mod.codeof;
char newchar (int) = string_mod.newchar;
char, int charat (string, int) = string_mod.charat;
int strlen (string) = string_mod.length;
string substr (string, int, int) = string_mod.slice;
string btos (buffer) = string_mod.`new`;
string addch (string, char) = string_mod.add;
buffer strbuf (string) = string_mod.tobuffer;

type buffer = buffer_mod.buffer;
buffer newbuf (int size) = buffer_mod.`new`;
int bufsize (buffer) = buffer_mod.size;
int bufget (buffer, int i) = buffer_mod.get;
void bufset (buffer, int i, int val) = buffer_mod.set;

type osfile = io.file;
type osmode = io.mode;
osmode osw () = io.w;
osfile osopen (string, osmode) = io.open;
buffer read (osfile, int) = io.read;
void oswrite (osfile, buffer) = io.write;
void osclose (osfile) = io.close;

int or (int, int) = intbit.or;
int and (int, int) = intbit.and;
int shl (int, int) = intbit.shl;
int shr (int, int) = intbit.shr;

module _array$<T> = import utils.arraylist(module{`0`=T;});
type Array$<T> = _array$<T>.``;
extend Array$<T> {
    Array$<T> new () = _array$<T>.new;
    void push (Array$<T>, T) = _array$<T>.push;
    int len (Array$<T>) = _array$<T>.len;
    T get (Array$<T>, int) = _array$<T>.get;
}

private module _map$<T> = (import utils.stringmap)(module{`0`=T;});
type Map$<T> = _map$<T>.``;
extend Map$<T> {
    Map$<T> new () = _map$<T>.new;
    void set (Map$<T>, string, T) = _map$<T>.set;
    T? get (Map$<T>, string) = _map$<T>.get;
}


import aurodump.items.Unit;
import aurodump.items.SubUnit;
import aurodump.items.Function;
import aurodump.items.Type;
import aurodump.items.Module;
import aurodump.items.MetaNode;

import aurodump.items.Item;
import aurodump.items.PartialItem;
import aurodump.items.SourceInfo;
import aurodump.items.MetaNodeIter;
import aurodump.items.ImportedItem;
import aurodump.items.ConstantCall;
import aurodump.items.ModuleArgument;

import aurodump.code.Code;
import aurodump.code.Instruction;

extend Unit {
    Array$<Module> modules$get (Unit) = (import aurodump.items).modules$get$Unit;
    Array$<Type> types$get (Unit) = (import aurodump.items).types$get$Unit;
    Array$<Function> functions$get (Unit) = (import aurodump.items).functions$get$Unit;
}

extend Function {
    Array$<Type> ins$get (Function) = (import aurodump.items).ins$get$Function;
    Array$<Type> outs$get (Function) = (import aurodump.items).outs$get$Function;
}

extend Unit {
    Array$<SubUnit> subunits$get (Unit) = (import aurodump.items).subunits$get$Unit;
}

extend Code {
    Array$<Instruction> instructions$get (Code) = (import aurodump.code).instructions$get$Code;
}




private int _MAX () { return 64*8; }
private const int MAX = _MAX();

record Part {
  buffer buf;
  int size;
  any _next;
}

extend Part {
    Part new () {
        return Part{ newbuf(MAX), 0, any{false} };
    }

    Part? next (Part this) {
        if (this._next is Part)
            return Part?.new(this._next as Part);
        return null Part;
    }

    void setNext (Part this, Part next) {
        this._next = any{next};
    }
}



record Writer {
  Part first;
  Part last;
  int count;
}

extend Writer {
  Writer create () {
    Part part = Part.new();
    return Writer{part, part, 0};
  }

  void byte (Writer this, int n) {
    Part part = this.last;
    if (part.size >= MAX) {
      Part next = Part.new();
      part.setNext(next);

      // WHY DO IT NOT WORK????
      //this.last = next;
      //this.count = this.count + 1;
      Writer.last$set(this, next);
      Writer.count$set(this, Writer.count$get(this) + 1);

      part = next;
    }

    bufset(part.buf, part.size, n);
    part.size = part.size + 1;
  }

  void _num (Writer this, int n) {
    if (n > 127) this._num(shr(n, 7));
    this.byte(or(and(n, 127), 128));
  }

  void num (Writer this, int n) {
    if (n > 127) this._num(shr(n, 7));
    this.byte(and(n, 127));
  }

  void bytes (Writer this, buffer buf) {
    int i = 0;
    while (i < bufsize(buf)) {
      this.byte(bufget(buf, i));
      i = i+1;
    }
  }

  void rawstr (Writer this, string s) {
    this.bytes(strbuf(s));
  }

  void str (Writer this, string s) {
    this.num(strlen(s));
    this.rawstr(s);
  }

  buffer tobuffer (Writer this) {
    buffer buf = newbuf((this.count * MAX) + this.last.size);
    Part part = this.first;
    int i = 0;
    loop:
      int j = 0;
      while (j < part.size) {
        bufset(buf, i, bufget(part.buf, j));
        j = j+1;
        i = i+1;
      }
      Part? next = part.next();
      if (!next is null) {
        part = next.get();
        goto loop;
      }
    return buf;
  }
}

void writeModule (Writer w, Module it) {
    import aurodump.items.DefineItem;
    import aurodump.items.ModuleNull;
    import aurodump.items.ModuleImport;
    import aurodump.items.ModuleDefine;
    import aurodump.items.ModuleBuild;
    import aurodump.items.ModuleFunctor;
    import aurodump.items.ModuleFunctorArg;

    extend ModuleDefine {
        Array$<DefineItem> items$get (ModuleDefine) = (import aurodump.items).items$get$ModuleDefine;
    }

    any inner = it.inner;

    if (inner is ModuleNull) {
        w.byte(0);
    } else if (inner is ModuleImport) {
        w.byte(1);
        w.str((inner as ModuleImport).name);
    } else if (inner is ModuleDefine) {
        ModuleDefine inner = inner as ModuleDefine;
        w.byte(2);

        w.num(inner.items.len());

        int j = 0;
        while (j < inner.items.len()) {
            DefineItem entry = inner.items.get(j);
            w.num(entry.item.kindInt());
            w.num(entry.item.index());
            w.str(entry.name);
            j = j+1;
        }
    } else if (inner is ImportedItem) {
        ImportedItem inner = inner as ImportedItem;
        w.byte(3);
        w.num(inner.mod.index);
        w.str(inner.name);
    } else if (inner is ModuleBuild) {
        ModuleBuild inner = inner as ModuleBuild;
        w.byte(4);
        w.num(inner.base.index);
        w.num(inner.argument.index);
    } else if (inner is SubUnit) {
        // Declares the module and SubUnit.
        // Contents are written after the root subunit.
        w.byte(5);
    } else if (inner is ModuleArgument) {
        // Module Arguments are implicit (first module of a subunit)
    } else if (false) {
        println("Warning: functor modules not supported in 0.8");
        w.byte(0);
    } else {
        println("Warning: Cannot serialize module " + it.toString());
        w.byte(0);
    }
}

void writeType (Writer w, Type it) {
    if (it.def is null) {
        w.byte(0);
    } else {
        ImportedItem inner = it.def.get();
        w.num(inner.mod.index + 1);
        w.str(inner.name);
    }
}

void writeFunction (Writer w, Function it) {
    import aurodump.items.ModuleNull;

    // kind
    if (it.inner is ModuleNull) {
        w.byte(0);
    } else if (it.inner is Code) {
        w.byte(1);
    } else if (it.inner is ImportedItem) {
        ImportedItem inner = it.inner as ImportedItem;
        w.num(inner.mod.index + 2);
        // Write the name after the arguments
    } else {
        println("Warning: cannot serialize function " + it.toString());
        w.byte(0);
    }

    // ins
    w.num(it.ins.len());
    int i = 0;
    while (i < it.ins.len()) {
        w.num(it.ins.get(i).index);
        i = i+1;
    }

    // outs
    w.num(it.outs.len());
    int i = 0;
    while (i < it.outs.len()) {
        w.num(it.outs.get(i).index);
        i = i+1;
    }

    if (it.inner is ImportedItem) {
        ImportedItem inner = it.inner as ImportedItem;
        w.str(inner.name);
    }
}

void writeConstant (Writer w, Function it) {
    import aurodump.items.ConstantInt;
    import aurodump.items.ConstantBin;
    import aurodump.items.ConstantCall;

    extend ConstantCall {
        Function function$get (ConstantCall) = (import aurodump.items).function$get$ConstantCall;
        Array$<Function> args$get (ConstantCall) = (import aurodump.items).args$get$ConstantCall;
    }

    if (it.inner is ConstantInt) {
        w.num(1);
        w.num((it.inner as ConstantInt).value);
    }
    if (it.inner is ConstantBin) {
        w.num(2);
        buffer value = (it.inner as ConstantBin).value;
        w.num(bufsize(value));
        w.bytes(value);
    }
    if (it.inner is ConstantCall) {
        ConstantCall inner = it.inner as ConstantCall;

        /*buffer buf = newbuf(6);
        bufset(buf, 0, inner.function.index);
        bufset(buf, 1, 42);
        w.num(2); w.num(6); w.bytes(buf);
        return;*/

        w.num(inner.function.index + 16);

        int i = 0;
        while (i < inner.args.len()) {
            w.num(inner.args.get(i).index);
            //w.num(0);
            i = i+1;
        }
    }
}

void writeCode (Writer w, Function fn, Code code) {
    import aurodump.code.Reg;
    import aurodump.code.Label;

    import aurodump.code.CallInst;
    import aurodump.code.EndInst;
    import aurodump.code.MovInst;
    import aurodump.code.JmpInst;
    import aurodump.code.JifInst;
    import aurodump.code.HltInst;


    extend CallInst {
        Function function$get (CallInst) = (import aurodump.code).function$get$CallInst;
        Array$<Reg> args$get (CallInst) = (import aurodump.code).args$get$CallInst;
        Array$<Reg> outs$get (CallInst) = (import aurodump.code).outs$get$CallInst;
    }

    extend EndInst {
        Array$<Reg> args$get (EndInst) = (import aurodump.code).args$get$EndInst;
    }

    //w.num(1); w.num(1); return;

    // Maps: itos(label_index) => instruction_index
    Map$<int> labelMap = Map$<int>.new();

    int getLabelValue (Map$<int> labelMap, Label lbl) {
        int? _val = labelMap.get(itos(lbl.index));
        if (_val is null) return -1;
        return _val.get();
    }

    int count = 0;
    int i = 0;
    while (i < code.instructions.len()) {
        Instruction inst = code.instructions.get(i);
        if (inst.inner is Label) {
            Label inner = inst.inner as Label;

            labelMap.set(itos(inner.index), count);
        } else {
            count = count+1;
        }
        i = i+1;
    }

    w.num(count);

    int i = 0;
    while (i < code.instructions.len()) {
        Instruction inst = code.instructions.get(i);
        any inner = inst.inner;

        // Usually at end of loop, but the branches below skip it when they continue
        i = i+1;

        /*if (inner is CallInst) {
            w.num(5);
            w.num((inner as CallInst).function.index);
        } else if (true) {
            w.num(2);
        }

        else*/ if (inner is Reg) {
            Reg inner = inner as Reg;
            w.num(2);
        }

        else if (inner is MovInst) {
            MovInst inner = inner as MovInst;
            w.num(4);
            w.num(inner.dst.index);
            w.num(inner.src.index);
        }

        else if (inner is JmpInst) {
            JmpInst inner = inner as JmpInst;

            int? _lbl = labelMap.get(itos(inner.label.index));
            if (_lbl is null) {
                // BAD LABEL
                w.num(1);
            } else {
                w.num(5);
                w.num(_lbl.get());
            }
        }

        else if (inner is JifInst) {
            JifInst inner = inner as JifInst;

            int? _lbl = labelMap.get(itos(inner.label.index));
            if (_lbl is null) {
                // BAD LABEL
                w.num(1);
            } else {
                if (inner.isNif) w.num(7);
                else w.num(6);

                w.num(_lbl.get());
                w.num(inner.cond.index);
            }

            
        }

        else if (inner is CallInst) {
            CallInst inner = inner as CallInst;

            //w.num(2);
            w.num(inner.function.index + 16);

            /*
            string s = "";
            if (inner.outs.len() > 0) {
                int i = 0;
                while (i < inner.outs.len()) {
                    if (i > 0) s = s + ", ";
                    s = s + inner.outs.get(i).toString();
                    i = i+1;
                }
                s = s + " = ";
            }
            */

            int i = 0;
            while (i < inner.args.len()) {
                w.num(inner.args.get(i).index);
                i = i+1;
            }
        }

        else if (inner is EndInst) {
            EndInst inner = inner as EndInst;
            w.num(0);

            int i = 0;
            while (i < inner.args.len()) {
                w.num(inner.args.get(i).index);
                i = i+1;
            }
        }
        else if (inner is HltInst) {
            w.num(1);
        }

        else if (inner is Label) {
            // Skip
        }

        else {
            error("Unknown instruction" + inst.toString());
        }
    }
}

void writeSubunit (Writer w, SubUnit unit) {
    // All SubUnit first modules are implicit.
    // For root, module #0 is not used. For non-root, its the argument.
    w.num(unit.modules.len() - 1);
    int i = 1;
    while (i < unit.modules.len()) {
        writeModule(w, unit.modules.get(i));
        i = i+1;
    }

    w.num(unit.types.len());
    int i = 0;
    while (i < unit.types.len()) {
        writeType(w, unit.types.get(i));
        i = i+1;
    }

    w.num(unit.functions.len());
    int i = 0;
    while (i < unit.functions.len()) {
        Function fn = unit.functions[i];
        writeFunction(w, fn);
        i = i+1;
    }

    w.num(unit.constants.len());
    int i = 0;
    while (i < unit.constants.len()) {
        Function fn = unit.constants.get(i);
        writeConstant(w, fn);
        i = i+1;
    }

    int i = 0;
    while (i < unit.functions.len()) {
        Function fn = unit.functions[i];
        if (fn.inner is Code) {
            writeCode(w, fn, fn.inner as Code);
        }
        i = i+1;
    }

    // Metadata
    w.num(0);
}

void writeUnit (Unit unit, string path) {
    Writer w = Writer.create();

    w.rawstr("Auro 0.8"); // 0.8
    w.byte(0);

    writeSubunit(w, unit.root);
    // TODO: Write subunits

    int i = 0;
    while (i < unit.subunits.len()) {
        writeSubunit(w, unit.subunits.get(i));
        i = i+1;
    }

    buffer buf = w.tobuffer();

    osfile f = osopen(path, osw());
    oswrite(f, buf);
    osclose(f);
}