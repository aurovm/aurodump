# Aurodump (Geber? hehe)

Shows the contents of an auro module file and the bytes that correspond to everything in it.

To install aurodump, clone the repo and run `sudo make install`.

## Alternative version

This repo also has an alternative version of aurodump, written in Aulang. It's designed to be more abstract and more programatically usable, to do more complex analysis about modules.

At the moment it just shows the contents in a format closer to aulang source code and with readable identifiers.

To install this version first install [auropm](https://gitlab.com/aurovm/auropm), then run `auropm install` in the cloned directory. After that you can use it anywhere with `nugget aurodump`.